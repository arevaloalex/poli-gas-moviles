## Poligas

Poligas es la nueva nueva app que te permite obtener tu cilindro de gas a domicilio r�pido y en simples pasos.

>Harto de esperar a que el distribuidor pase por tu casa y salir corriendo para que no se vaya, Poligas es la soluci�n para este problema.

Puedes solicitar tu cilindro de gas en simples pasos, adem�s de dar *seguimiento al pedido*, para que puedas ver en tiempo real el estado de tu pedido.
Si no deseas esperar a tu pedido puedes **agendarlo**, de esta manera tendras tu cilindro de gas en la hora y fecha que tu prefieras, asi no te quedaras sin tu cilindro de gas.

#### Caracteristicas

`Pedido de cilindro de gas express
Agendar un pedido de cilindro de gas
Puedes crear alertas que te recuerden pedir tu cilindro
Descuentos`

Este proyecto es desarrollado por un grupo de estudiantes de la Escuela Polit�cnica Nacional din�micos y trabajadores. Nuestro objetivo es seguir aprendiendo y fortaleciendo nuestras fortalezas en el �mbito de desarrollo de software.


